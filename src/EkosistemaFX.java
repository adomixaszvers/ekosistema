/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import ecosystem.Config;
import static ecosystem.Singleton.getInstance;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.util.Duration;
import static javafx.util.Duration.millis;

/**
 *
 * @author adomas
 */
public class EkosistemaFX extends Application {

    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     *
     * @param primaryStage
     */
    @Override
    public void start(Stage primaryStage) {

        Group root = getInstance().root;

        Scene scene = new Scene(root, Config.canvas_width, Config.canvas_height);
        
        Timeline timeline = new Timeline();
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.getKeyFrames().add(new KeyFrame(millis(25),new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent t) {
                getInstance().evolve(); //To change body of generated methods, choose Tools | Templates.
            }
        }));
        timeline.play();
        primaryStage.setTitle("Ecosystem!");
        primaryStage.setScene(scene);
        primaryStage.show();
        /*getInstance().moveAll();
         getInstance().drawAll();*/
    }

}
