/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.animal;

import ecosystem.Config;
import static ecosystem.Config.canvas_width;
import ecosystem.World;
import ecosystem.graphics.Position;
import ecosystem.graphics.Vector;
import ecosystem.graphics.velocity.Velocity;
import ecosystem.organism.Organism;
import static java.lang.Math.random;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author adomas
 */
public class Herbivore extends Animal {

    private final static double dt = 0.04,
            end_of_child = 2,
            end_of_puberty = 5,
            end_of_adult = 120,
            changeble_span = 2;

    private Organism enemy, friend;

    /**
     *
     * @param p
     * @param energy
     */
    public Herbivore(Position p, int energy, World world) {
        super(p, new Velocity(5, 0), energy);
        this.getVelocity().randomRotate();
        this.setSize(8);
        this.setEnergy(energy);
        this.enemy = null;
        this.friend = null;
        Circle circle = new Circle(getPosition().getX(), getPosition().getY(), getSize(), Color.BLUE);
        this.setShape(circle);
        this.world = world;
    }

    @Override
    public void draw() {
        ((Circle) this.getShape()).setCenterX(getPosition().getX());
        ((Circle) this.getShape()).setCenterY(getPosition().getY());
        ((Circle) this.getShape()).setRadius(getSize());
    }

    /**
     *
     */
    public void move_option() {
        this.setEnergy(this.getEnergy() - 0.1);
        if (this.getState().isAdult()) {
            this.setEnergy(this.getEnergy() - 0.3);
        }
        if (random() < 0.1) {
            this.getVelocity().randomRotateWith(45);
        }
        if (this.enemy != null && !world.isExist(enemy)) {
            this.enemy = null;
        } else if (!this.inSight(enemy)) {
            this.enemy = null;
        }
        if (this.enemy != null && !this.getState().isChild()) {
            Vector result = this.enemy.getPosition().clone().subtract(this.getPosition()).setLength(this.getVelocity().getLength()).rotate(180);
            this.getVelocity().setVector(result.getX(), result.getY());
        } else if (random() < 0.1) {
            Organism target=null;
            target=this.findClosest(world.getPlant());
            if (target != null) {
                Vector result = target.getPosition().clone().subtract(this.getPosition()).setLength(this.getVelocity().getLength());
                this.getVelocity().setVector(result.getX(), result.getY());
            }
        }

    }

    /**
     *
     */
    public void eat_option() {
        this.setEnergy(this.getEnergy() + 20);
    }

    /**
     *
     */
    public void grow_if() {
        if (this.getAge() < Herbivore.end_of_child) {
            this.getState().setState("child");
            this.setSize(4);
            this.getVelocity().normalize().extend(4);
        } else if (this.getState().isChild() && 60 < this.getEnergy() && Herbivore.end_of_child < this.getAge()) {
            this.getState().grow();
            this.setSize(5);
            this.getVelocity().normalize().extend(5);
            this.setEnergy(this.getEnergy() - 20);
        } else if (this.getState().isPuberty() && 70 < this.getEnergy() && Herbivore.end_of_puberty < this.getAge()) {
            this.getState().grow();
            this.setSize(8);
            this.getVelocity().normalize().extend(6);
            this.setEnergy(this.getEnergy() - 30);
        } else if (!this.getState().isOld() && Herbivore.end_of_adult < this.getAge()) {
            this.getState().setState("old");
            this.getVelocity().normalize().extend(1);
        }
        this.setAge(this.getAge() + Herbivore.dt);
    }

    @Override
    public Herbivore birth_if() {
        if (Config.herbivore_energy_for_child <= this.getEnergy() && this.getAge() < Herbivore.end_of_adult) {
            this.setEnergy(this.getEnergy() - 70);
            Herbivore herbivore = new Herbivore(this.getPosition().clone(), 50, this.world);
            return herbivore;
        }
        return null;
    }

    @Override
    public String instanceOf() {
        return "Herbivore";
    }
    
    @Override
    public void move() {
        this.getPosition().add(this.getVelocity());

        if (this.getPosition().getX() < -10) {
            this.getPosition().setX(canvas_width - 10);
        } else if (canvas_width + 10 < this.getPosition().getX()) {
            this.getPosition().setX(0);
        }
        if (this.getPosition().getY() < -10) {
            this.getPosition().setY(Config.canvas_height - 10);  // to fix ecosystem field as square
        } else if (Config.canvas_height + 10 < this.getPosition().getY()) {
            this.getPosition().setY(10);
        }

        this.move_option();
    }

}
