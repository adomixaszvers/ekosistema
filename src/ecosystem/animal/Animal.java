/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.animal;

import ecosystem.Config;
import static ecosystem.Config.canvas_width;
import ecosystem.State;
import ecosystem.graphics.Position;
import ecosystem.graphics.Vector;
import ecosystem.graphics.velocity.Velocity;
import ecosystem.organism.Organism;
import static java.lang.Math.floor;
import java.util.ArrayList;

/**
 *
 * @author adomas
 */
public class Animal extends Organism {

    private Velocity v;
    private double age;
    private double sightLength;
    private double sightAngle;
    private double sightWidth;
    private State state;

    /**
     *
     * @param p
     * @param v
     * @param energy
     */
    public Animal(Position p, Velocity v, double energy) {
        super(p, energy, 5);
        //this.setPosition(p.clone());
        this.v = v.clone();
        this.age = 0;
        this.sightLength = 10;
        this.sightAngle = Math.PI / 4;
        this.sightWidth = Math.PI / 2;
        this.state = new State();
    }

    /**
     *
     */
    /**
     *
     * @return
     */
    public Vector calculateHead() {
        return this.getPosition().clone().add(this.v.clone().setLength(this.getSize()));
    }

    /**
     *
     */
    public void move() {
        this.getPosition().add(this.v);

        if (this.getPosition().getX() < -10) {
            this.getPosition().setX(canvas_width - 10);
        } else if (canvas_width + 10 < this.getPosition().getX()) {
            this.getPosition().setX(0);
        }
        if (this.getPosition().getY() < -10) {
            this.getPosition().setY(Config.canvas_height - 10);  // to fix ecosystem field as square
        } else if (Config.canvas_height + 10 < this.getPosition().getY()) {
            this.getPosition().setY(10);
        }

        this.move_option();
    }

    /**
     *
     * @param liv1
     * @param liv2
     * @return
     */
    public Organism closer(Organism liv1, Organism liv2) {
        if (liv1.getPosition().distanceBetween(this.getPosition()) < liv2.getPosition().distanceBetween(this.getPosition())) {
            return liv1;
        } else {
            return liv2;
        }
    }

    /**
     *
     * @param liv_ary
     * @return
     */
    public Organism findClosest(ArrayList<Organism> liv_ary) {

        ArrayList<Organism> options = this.getAllInSight(liv_ary);

        if (0 < options.size()) {

            Organism closest_liv = options.get(0);
            for (int i = 0; i < options.size(); i++) {
                closest_liv = closer(closest_liv, options.get(i));
            }
            return closest_liv;
        }
        return null;
    }

    /**
     *
     * @param liv_ary
     * @return
     */
    public Organism findClosestNoMe(ArrayList<Organism> liv_ary) {

        ArrayList<Organism> options = this.getAllInSight(liv_ary);

        if (0 < options.size()) {

            Organism closest_liv = options.get(0);
            for (int i = 0; i < options.size(); i++) {
                Organism tem = closer(closest_liv, options.get(i));
                if (tem != this) {
                    closest_liv = tem;
                }
            }
            return closest_liv;
        }
        return null;
    }

    /**
     *
     * @param liv
     * @return
     */
    public boolean inSight(Organism liv) {
        if (liv != null) {
            return (this.getPosition().distanceBetween(liv.getPosition()) < this.getSightLength()
                    && this.inSightAngle(liv.getPosition().getAngleAround(this.getPosition())))
                    || this.getPosition().distanceBetween(liv.getPosition()) < 2 * this.getSize();
        } else {
            return false;
        }
    }

    /**
     *
     * @param angle
     * @return
     */
    public boolean inSightAngle(double angle) {
        Vector head = this.calculateHead();
        double lowerbound = head.getAngleAround(this.getPosition()) - this.getSightWidth() / 2;
        double upperbound = head.getAngleAround(this.getPosition()) + this.getSightWidth() / 2;
        return lowerbound < angle && angle < upperbound;
    }

    ArrayList<Organism> getAllInSight(ArrayList<Organism> liv_ary) {
        ArrayList<Organism> targets = new ArrayList<>();
        for (int i = 0; i < liv_ary.size(); i++) {
            if (this.inSight(liv_ary.get(i))) {
                targets.add(targets.size(), liv_ary.get(i));
            }
        }
        return targets;
    }

    /**
     *
     * @param liv_ary
     * @return
     */
    public ArrayList<Organism> eat(ArrayList<Organism> liv_ary) {
        if (liv_ary.size() == 0) {
            return liv_ary;
        }
        int index = binarySearch(liv_ary, 0, liv_ary.size() - 1);
        if (0 < index) {
            world.root.getChildren().remove(liv_ary.get(index).getShape());
            //System.out.println("Suvalgytas "+liv_ary.get(index).instanceOf());
            liv_ary.remove(index);
            this.eat_option();
        }
        return liv_ary;
    }

    /**
     *
     * @param liv_ary
     * @param lower
     * @param upper
     * @return
     */
    public int binarySearch(ArrayList<Organism> liv_ary, int lower, int upper) {
        int index = (int) floor((upper + lower) / 2);
        double dis = liv_ary.get(index).getPosition().distanceBetween(this.getPosition());

        if (dis < this.getSize()) {
            return index;
        }
        if (upper == lower) {
            return -1;
        } else if (liv_ary.get(index).getPosition().getX() < this.getPosition().getX()) {
            lower = index + 1;
        } else {
            upper = index;
        }
        return this.binarySearch(liv_ary, lower, upper);
    }

    /**
     *
     */
    public void grow() {
        this.grow_if();
    }

    /**
     *
     * @return
     */
    /*@Override
     public Organism birth() {
     return this.birth_if();
     }*/
    /**
     *
     * @param v
     */
    public void setVelocity(Velocity v) {
        this.v = v;
    }

    /**
     *
     * @return
     */
    public Velocity getVelocity() {
        return this.v;
    }

    /**
     *
     * @param age
     */
    public void setAge(double age) {
        this.age = age;
    }

    /**
     *
     * @return
     */
    public double getAge() {
        return this.age;
    }

    /**
     *
     * @return
     */
    public State getState() {
        return this.state;
    }

    /**
     *
     * @param s
     */
    public void setState(State s) {
        this.state = s.clone();
    }

    @Override
    public String instanceOf() {
        return "Animal";
    }

    protected void eat_option() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected void move_option() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected void graphics(Vector calculateHead) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    protected void grow_if() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * @return the sightLength
     */
    public double getSightLength() {
        return sightLength;
    }

    /**
     * @param sightLength the sightLength to set
     */
    public void setSightLength(double sightLength) {
        this.sightLength = sightLength;
    }

    /**
     * @return the sightAngle
     */
    public double getSightAngle() {
        return sightAngle;
    }

    /**
     * @param sightAngle the sightAngle to set
     */
    public void setSightAngle(double sightAngle) {
        this.sightAngle = sightAngle;
    }

    /**
     * @return the sightWidth
     */
    public double getSightWidth() {
        return sightWidth;
    }

    /**
     * @param sightWidth the sightWidth to set
     */
    public void setSightWidth(double sightWidth) {
        this.sightWidth = sightWidth;
    }

}
