/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.animal;

import ecosystem.Config;
import static ecosystem.Config.canvas_width;
import ecosystem.World;
import ecosystem.graphics.Position;
import ecosystem.graphics.Vector;
import ecosystem.graphics.velocity.Velocity;
import ecosystem.organism.Organism;
import static java.lang.Math.random;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author adomas
 */
public class Carnivore extends Animal {
    
    private final static double dt = 0.04,
            end_of_child = 20,
            end_of_puberty = 40,
            end_of_adult = 600,
            changeble_span = 0.5;
    private Animal target;
    private double v_count;

    /**
     *
     * @param p
     * @param energy
     */
    public Carnivore(Position p, double energy, World world) {
        super(p, new Velocity(8, 0), energy);
        this.getVelocity().randomRotate();
        this.setSize(10);
        this.setSightLength(100);
        this.setSightWidth(Math.PI / 4);
        this.setSightAngle(Math.PI / 2);
        this.target = null;
        this.v_count = 0;
        Circle circle = new Circle(getPosition().getX(), getPosition().getY(), getSize(), Color.RED);
        this.setShape(circle);
        this.world = world;
    }

    /**
     *
     */
    @Override
    public void move_option() {
        this.v_count += Carnivore.dt;
        if (this.getState().isChild()) {
            this.setEnergy(this.getEnergy() - 0.2);
        } else if (this.getState().isAdult()) {
            this.setEnergy(this.getEnergy() - 12);
        } else {
            this.setEnergy(this.getEnergy() - 5);
        }
        if (!this.getState().isAdult() && random() < 0.1) {
            this.getVelocity().randomRotateWith(90);
        }
        if (this.target != null && !world.isExist(this.target)) {
            target = null;
        }
        if (this.target == null || this.target.die()) {
            if (this.getState().isPuberty() || this.getState().isAdult()) {
                this.target = (Animal) this.findClosest(world.getHerbivore());
                this.changeVelocityTo(this.target);
                this.v_count = 0;
            }
        } else if (this.inSight(this.target) && !this.target.die()) {
            this.changeVelocityTo(target);
        } else {
            this.setVelocity(new Velocity(8, 0));
            this.getVelocity().randomRotate();
            target = null;
            this.v_count = 0;
        }
    }
    
    private void changeVelocityTo(Animal target) {
        if (target != null) {
            //target.print();System.out.println();
            Vector result = target.getPosition().clone().subtract(this.getPosition()).setLength(this.getVelocity().getLength());
            this.setVelocity(new Velocity(result.getX(), result.getY()));
        }
    }

    /**
     *
     */
    @Override
    public void eat_option() {
        this.setEnergy(this.getEnergy() + 250);
        this.target = null;
    }

    /**
     *
     */
    @Override
    public void grow_if() {
        if (this.getAge() < Carnivore.end_of_child) {
            this.getState().setState("child");
            this.setSize(5);
            this.getVelocity().normalize().extend(6);
        } else if (this.getState().isChild() && 100 < this.getEnergy() && Carnivore.end_of_child < this.getAge()) {
            this.getState().grow();
            this.setSize(7);
            this.getVelocity().normalize().extend(7);
            this.setEnergy(this.getEnergy() - 50);
        } else if (this.getState().isPuberty() && 2_000 < this.getEnergy() && Carnivore.end_of_puberty < this.getAge()) {
            this.getState().grow();
            this.setSize(10);
            this.getVelocity().normalize().extend(9);
            this.setEnergy(this.getEnergy() - 800);
            this.setAge(Carnivore.end_of_adult - random() * 10);
        } else if (!this.getState().isOld() && Carnivore.end_of_adult < this.getAge()) {
            this.getState().setState("old");
            this.getVelocity().normalize().extend(1);
        }
        
        this.setAge(this.getAge() + Carnivore.dt);
    }
    
    @Override
    public Organism birth_if() {
        if (this.getState().isAdult() && Config.cornivore_energy_for_child <= this.getEnergy()) {
            this.setEnergy(this.getEnergy() - 1_250);
            Carnivore carnivore = new Carnivore(this.getPosition().clone(), 100, this.world);
            return carnivore;
        }
        return null;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean die() {
        if (this.getEnergy() <= 0) {
            world.root.getChildren().remove(this.getShape());
            return true;
        }
        return this.getState().isOld() && random() < 0.01;
    }
    
    @Override
    public void draw() {
        ((Circle) this.getShape()).setCenterX(getPosition().getX());
        ((Circle) this.getShape()).setCenterY(getPosition().getY());
        ((Circle) this.getShape()).setRadius(getSize());
    }
    
    @Override
    public void move() {
        this.getPosition().add(this.getVelocity());

        if (this.getPosition().getX() < -10) {
            this.getPosition().setX(canvas_width - 10);
        } else if (canvas_width + 10 < this.getPosition().getX()) {
            this.getPosition().setX(0);
        }
        if (this.getPosition().getY() < -10) {
            this.getPosition().setY(Config.canvas_height - 10);  // to fix ecosystem field as square
        } else if (Config.canvas_height + 10 < this.getPosition().getY()) {
            this.getPosition().setY(10);
        }

        this.move_option();
    }
    
    @Override
    public String instanceOf() {
       return "Carnivore"; 
    }
}
