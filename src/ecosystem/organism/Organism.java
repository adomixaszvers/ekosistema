/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.organism;

import ecosystem.World;
import ecosystem.graphics.Position;
import javafx.scene.shape.Shape;

/**
 *
 * @author adomas
 */
public class Organism implements Comparable<Organism>, Cloneable {

    /**
     *
     */
    public  World world;

    private Position p;
    private double energy;
    private double size;
    private Shape shape;

    /**
     *
     * @param p
     * @param energy
     * @param size
     * @
     */
    public Organism(Position p, double energy, double size) {
        this.p = p.clone();
        this.energy = energy;
        this.size = size;
        //this.world = getInstance();
    }

    /**
     *
     * @
     */
    public Organism() {
        this(new Position(), 0, 0);
        //this.world=getInstance();
    }

    /**
     *
     */
    /**
     *
     */
    public void draw() {
        
    }

    /**
     *
     * @return
     */
    public Organism birth_if() {
        return null;
    }

    /**
     * @return the p
     */
    public Position getPosition() {
        return p;
    }

    /**
     * @param p the p to set
     */
    public void setPosition(Position p) {
        this.p = p;
    }

    /**
     * @return the energy
     */
    public double getEnergy() {
        return energy;
    }

    /**
     * @param energy the energy to set
     */
    public void setEnergy(double energy) {
        this.energy = energy;
    }

    /**
     * @return the size
     */
    public double getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(double size) {
        this.size = size;
    }

    /**
     *
     * @return
     */
    public double getX() {
        return this.p.getX();
    }

    /**
     *
     * @return
     */
    public double getY() {
        return this.p.getY();
    }

    /**
     *
     * @return
     */
    public String instanceOf() {
        return "Organism";
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return this.instanceOf() + "( positon " + this.p.toString() + ", energy " + this.energy + ", size " + this.size + " )";
    }

    /**
     *
     */
    public void println() {
        System.out.println(this.toString());
    }

    @Override
    public Organism clone() {
        Organism organism = new Organism(p, energy, size);
        return organism;
    }

    @Override
    public int compareTo(Organism o) {
        return (int) (getPosition().distanceBetween(o.getPosition())*(this.getX()-o.getX()));
    }

    /**
     *
     * @return
     */
    public Organism birth() {
        Organism child = birth_if();
        if(child!=null) {
            /*child.print();
            System.out.println();*/
            return child;
        } else {
            return null;
        }
    }

    /**
     *
     * @return
     */
    public boolean die() {
        if(this.getEnergy() <= 0) {
            world.root.getChildren().remove(this.getShape());
            return true;
        }
        return false;
    }

    /**
     * @return the shape
     */
    public Shape getShape() {
        return shape;
    }

    /**
     * @param shape the shape to set
     */
    public void setShape(Shape shape) {
        this.shape = shape;
    }
}
