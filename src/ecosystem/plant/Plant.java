/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.plant;

import ecosystem.Config;
import ecosystem.World;
import ecosystem.graphics.Position;
import ecosystem.organism.Organism;
import static java.lang.Math.random;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

/**
 *
 * @author adomas
 */
public class Plant extends Organism {

    /**
     *
     * @param position
     * @param world
     */
    public Plant(Position position, World world) {
        super();
        this.setPosition(position);
        this.setSize(5);
        this.setEnergy(100);
        Circle circle = new Circle(this.getPosition().getX(), this.getPosition().getY(), this.getSize());
        circle.setFill(Color.GREEN);
        this.setShape(circle);
        this.world= world;
    }

    @Override
    public Organism birth_if() {
        if (random() < 0.1) {
            double radx = random() * Config.canvas_width;
            double rady = random() *Config.canvas_height;
            Plant plant = new Plant(new Position(radx, rady), this.world);
            world.root.getChildren().add(plant.getShape());
            return plant;
        }
        return null;
    }

    @Override
    public String instanceOf() {
        return "Plant";
    }
    
    

    @Override
    public void draw() {
        ((Circle) this.getShape()).setCenterX(getPosition().getX());
        ((Circle) this.getShape()).setCenterY(getPosition().getY());
        ((Circle) this.getShape()).setRadius(getSize());
    }
    public void grow() {
        this.setEnergy(this.getEnergy()-2);
        if(getEnergy()<=0) {
            world.root.getChildren().remove(getShape());
            world.getPlant().remove(this);
        }
    }
}
