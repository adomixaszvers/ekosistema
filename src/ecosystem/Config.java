/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem;

/**
 *
 * @author adomas
 */
public class Config {

    /**
     *
     */
    public final static double canvas_height = 600;

    /**
     *
     */
    public final static double canvas_width = 600;

    /**
     *
     */
    public final static int num_plant = 100,
            /**
             *
             */
            num_herbivore = 100,
            /**
             *
             */
            num_carnivore = 5;
    public final static int plants_per_generation = 15;
    public final static int herbivore_energy_for_child = 70;
    public final static int cornivore_energy_for_child = 1_450;

    public final static int fps = 40;
    private Config() {
    }
}
