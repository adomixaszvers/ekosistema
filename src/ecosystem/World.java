/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem;

import ecosystem.animal.Animal;
import ecosystem.animal.Carnivore;
import ecosystem.animal.Herbivore;
import ecosystem.graphics.Position;
import ecosystem.organism.Organism;
import ecosystem.plant.Plant;
import static java.lang.Math.random;
import java.util.ArrayList;
import static java.util.Collections.sort;
import javafx.scene.Group;

/**
 *
 * @author adomas
 */
public class World {

    private ArrayList<Organism> p;
    private ArrayList<Organism> h;
    private ArrayList<Organism> c;

    /**
     *
     */
    public Group root;

    /**
     *
     * @param num_plant
     * @param num_herbivore
     * @param num_carnivore
     */
    public World(int num_plant, int num_herbivore, int num_carnivore) {
        root = new Group();
        p = new ArrayList<>();
        h = new ArrayList<>();
        c = new ArrayList<>();

        for (int i = 0; i < num_plant; i++) {
            p.add(i, pconst());
        }

        for (int i = 0; i < num_herbivore; i++) {
            h.add(i, hconst());
        }

        for (int i = 0; i < num_carnivore; i++) {
            c.add(i, cconst());
        }
    }

    private Plant pconst() {
        Plant plant = new Plant(new Position(random() * Config.canvas_width, random() * Config.canvas_height), this);
        root.getChildren().add(plant.getShape());
        return plant;
    }

    private Herbivore hconst() {
        Herbivore herbivore = new Herbivore(new Position(random() * Config.canvas_width, random() * Config.canvas_height), 50, this);
        root.getChildren().add(herbivore.getShape());
        return herbivore;
    }

    private Carnivore cconst() {
        Carnivore carnivore = new Carnivore(new Position(random() * Config.canvas_width, random() * Config.canvas_height), 50, this);
        root.getChildren().add(carnivore.getShape());
        return carnivore;
    }

    /**
     *
     */
    public void evolve() {
        this.moveAll();
        this.eatAll();
        this.growAll();
        this.birthAll();
        this.dieAll();
        this.drawAll();
        System.out.println("Plants: "+getPlantPopulation());
        System.out.println("Herbivores "+getHerbivorePopulation());
        System.out.println("Carnivores: "+getCornivorePopulation());
    }

    public void moveAll() {
        for (int i = 0; i < h.size(); i++) {
            ((Herbivore) h.get(i)).move();
        }
        for (int i = 0; i < c.size(); i++) {
            ((Carnivore) c.get(i)).move();
        }
    }

    private void eatAll() {
        sort(p);
        sort(h);
        for (int i = 0; i < h.size(); i++) {
            p = ((Herbivore) h.get(i)).eat(p);
        }
        for (int i = 0; i < c.size(); i++) {
            h = ((Carnivore) c.get(i)).eat(h);
        }
    }

    private void growAll() {
        for (int i=p.size()-1; i>-1;i--) {
            ((Plant)p.get(i)).grow();
        }
        for (int i = 0; i < h.size(); i++) {
            ((Animal) h.get(i)).grow();
        }
        for (int i = 0; i < c.size(); i++) {
            ((Animal) c.get(i)).grow();
        }
    }

    private void birthAll() {
        for (int i = 0; i < 8; i++) {
            if (random() < 0.8) {
                Plant plant = new Plant(new Position(random() * Config.canvas_width, random() * Config.canvas_height), this);
                this.root.getChildren().add(plant.getShape());
                p.add(p.size(), plant);
                
            }
        }
        for (int i = 0; i < h.size(); i++) {
            Organism tem = h.get(i).birth();
            if (tem != null) {
                root.getChildren().add(tem.getShape());
                h.add(h.size(), tem);
            }
        }
        for (int i = 0; i < c.size(); i++) {
            Organism tem = c.get(i).birth();
            if (tem != null) {
                root.getChildren().add(tem.getShape());
                c.add(c.size(), tem);
            }
        }
    }

    private void dieAll() {
        for (int i = h.size() - 1; -1 < i; i--) {
            if (h.get(i).die()) {
                root.getChildren().remove(h.get(i).getShape());
                h.remove(i);
            }
        }
        for (int i = c.size() - 1; -1 < i; i--) {
            if (c.get(i).die()) {
                root.getChildren().remove(c.get(i).getShape());
                c.remove(i);
            }
        }
    }
    
    public void drawAll() {
        for (int i = 0; i < p.size(); i++) {
            p.get(i).draw();
        }
        for (int i = 0; i < h.size(); i++) {
            h.get(i).draw();
        }
        for (int i = 0; i < c.size(); i++) {
            c.get(i).draw();
        }
    }

    /**
     *
     * @return
     */
    public ArrayList<Organism> getPlant() {
        return p;
    }

    /**
     *
     * @return
     */
    public ArrayList<Organism> getHerbivore() {
        return h;
    }

    /**
     *
     * @return
     */
    public ArrayList<Organism> getCornivore() {
        return c;
    }

    /**
     *
     * @return
     */
    public int getPlantPopulation() {
        return p.size();
    }

    /**
     *
     * @return
     */
    public int getHerbivorePopulation() {
        return h.size();
    }

    /**
     *
     * @return
     */
    public int getCornivorePopulation() {
        return c.size();
    }

    /**
     *
     * @param liv
     * @return
     */
    public boolean isExist(Organism liv) {
        return p.contains(liv) || h.contains(liv) || c.contains(liv);
    }
}
