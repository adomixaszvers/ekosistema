/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem;

/**
 *
 * @author adomas
 */
public class State implements Cloneable {

    /**
     *
     */
    public final static int child = 0,
            /**
             *
             */
            puberty = 1,
            /**
             *
             */
            adult = 2,
            /**
             *
             */
            old = 3;
    private int state = child;

    /**
     *
     * @param str
     */
    public void setState(String str) {
        switch (str) {
            case "child":
                state = child;
                break;
            case "puberty":
                state = puberty;
                break;
            case "adult":
                state = adult;
                break;
            case "old":
                state = old;
                break;
        }
    }

    /**
     *
     * @param state
     */
    public void setState(int state) {
        if (child <= state && state <= old) {
            this.state = state;
        }
    }

    /**
     *
     */
    public void grow() {
        if (state != old) {
            state++;
        }
    }

    /**
     *
     * @return
     */
    public int getState() {
        return state;
    }

    /**
     *
     * @return
     */
    public boolean isChild() {
        return state == child;
    }

    /**
     *
     * @return
     */
    public boolean isPuberty() {
        return state == puberty;
    }

    /**
     *
     * @return
     */
    public boolean isAdult() {
        return state == adult;
    }

    /**
     *
     * @return
     */
    public boolean isOld() {
        return state == old;
    }

    @Override
    public State clone() {
        State new_state = new State();
        new_state.setState(getState());
        return new_state;
    }

}
