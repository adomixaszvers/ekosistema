/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem;

/**
 *
 * @author adomas
 */
public class Singleton {

    private final static World instance = new World(Config.num_plant, Config.num_herbivore, Config.num_carnivore);

    /**
     *
     * @return
     */
    public static World getInstance() {
        return instance;
    }

    private Singleton() {
    }
}
