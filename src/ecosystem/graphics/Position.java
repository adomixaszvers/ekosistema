/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.graphics;

/**
 *
 * @author adomas
 */
public class Position extends Vector implements Cloneable {

    /**
     *
     */
    public Position() {
        super(0, 0);
    }

    /**
     *
     * @param x
     * @param y
     */
    public Position(double x, double y) {
        super(x, y);
    }

    /**
     *
     * @param p
     */
    public Position(Position p) {
        super(p);
    }

    @Override
    public Position clone() {
        return new Position(this);
    }
}
