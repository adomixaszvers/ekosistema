/**
 *
 */
package ecosystem.graphics;

import static java.lang.Math.asin;
import static java.lang.Math.cos;
import static java.lang.Math.round;
import static java.lang.Math.sin;
import static java.lang.Math.sqrt;

/**
 * @author adomas
 *
 */
public class Vector implements Cloneable {

    /**
     *
     * @param angle
     * @return
     */
    public static double convertRadian(double angle) {
        return (angle / 360) * 2 * Math.PI;
    }

    private double x, y;

    /**
     *
     */
    public Vector() {
        this(0, 0);
    }

    /**
     *
     * @param x
     * @param y
     */
    public Vector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @param v
     */
    public Vector(Vector v) {
        this.x = v.getX();
        this.y = v.getY();
    }

    /**
     *
     * @param v
     * @return
     */
    public Vector add(Vector v) {
        this.x += v.getX();
        this.y += v.getY();
        return this;
    }

    /**
     *
     * @param v
     * @return
     */
    public Vector subtract(Vector v) {
        this.x -= v.getX();
        this.y -= v.getY();
        return this;
    }

    /**
     *
     * @param c
     * @return
     */
    public Vector extend(double c) {
        this.x *= c;
        this.y *= c;
        return this;
    }

    /**
     *
     * @return
     */
    public Vector normalize() {
        double c = this.getLength();
        this.x /= c;
        this.y /= c;
        return this;
    }

    /**
     *
     * @param angle
     * @return
     */
    public Vector rotate(double angle) {
        double rad = convertRadian(angle);
        double temx = this.x;
        double temy = this.y;
        this.x = cos(rad)*temx-sin(rad)*temx;
        this.y = sin(rad)*temx+cos(rad)*temy;
        return this;
    }

    /**
     *
     * @param v
     * @param angle
     */
    public void rotateAround(Vector v, double angle) {
        this.subtract(v);
        this.rotate(angle);
        this.add(v);
    }

    /**
     *
     * @param v
     * @param c
     */
    public void extendDistance(Vector v, double c) {
        this.subtract(v);
        this.extend(c);
        this.add(v);
    }

    /**
     *
     * @param v
     * @param c
     */
    public void setDistance(Vector v, double c) {
        this.subtract(v).setLength(c).add(v);
    }

    /**
     *
     * @param v
     * @return
     */
    public double distanceBetween(Vector v) {
        return sqrt((v.getX() - this.x) * (v.getX() - this.x) + (v.getY() - this.y) * (v.getY() - this.y));
    }

    /**
     *
     * @return
     */
    @Override
    public String toString() {
        return "(" + this.x + ", " + this.y + ")";
    }

    /**
     *
     */
        public void print() {
                System.out.print(this.toString());
    }

    /**
     *
     * @return
     */
    public String toRoundString() {
        return "(" + round(this.x) + ", " + round(this.y) + ")";
    }

    /**
     *
     */
    public void roundPrint() {
        System.out.print(this.toRoundString());
    }

    /**
     *
     * @param x
     * @param y
     */
    public void setVector(double x, double y) {
        this.x = x;
        this.y = y;
    }

    /**
     *
     * @param c
     * @return
     */
    public Vector setLength(double c) {
        this.normalize();
        this.extend(c);
        return this;
    }

    /**
     *
     * @return
     */
    public double getLength() {
        return sqrt(this.x * this.x + this.y * this.y);
    }

    /**
     *
     * @return
     */
    public double getAngle() {
        return asin(this.x / this.getLength());
    }

    /**
     *
     * @param v
     * @return
     */
    public double getAngleAround(Vector v) {
        this.subtract(v);
        double ang = this.getAngle();
        this.add(v);
        return ang;
    }

    /**
     *
     * @return
     */
    public double getVerticalAngle() {
        this.rotate(-Math.PI / 2);
        double ang = this.getAngle();
        this.rotate(Math.PI / 2);
        return ang;
    }

    /**
     *
     * @param v
     * @return
     */
    public double getVerticalAngleAround(Vector v) {
        this.rotate(-Math.PI);
        double ang = this.getAngleAround(v);
        this.rotate(Math.PI);
        return ang;
    }

    /**
     *
     * @return
     */
    public final double getX() {
        return x;
    }

    /**
     *
     * @param x
     */
    public final void setX(double x) {
        this.x = x;
    }

    /**
     *
     * @return
     */
    public final double getY() {
        return y;
    }

    /**
     *
     * @param y
     */
    public final void setY(double y) {
        this.y = y;
    }

    @Override
    public Vector clone() {
        return new Vector(this);
    }
    
    public void dummyMethod1() {
        
    }

    /**
     *
     * @param v
     * @ret             */
}
