/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecosystem.graphics.velocity;

import ecosystem.graphics.Vector;
import ecosystem.organism.Organism;
import static java.lang.Math.random;

/**
 *
 * @author adomas
 */
public class Velocity extends Vector implements Cloneable {

    /**
     *
     */
    public Velocity() {
        super(0, 0);
    }

    /**
     *
     * @param x
     * @param y
     */
    public Velocity(double x, double y) {
        super(x, y);
    }

    /**
     *
     * @param v
     */
    public Velocity(Velocity v) {
        super(v);
    }

    /**
     *
     * @return
     */
    public Velocity randomRotate() {
        this.rotate(random() * 360);
        return this;
    }

    /**
     *
     * @param rang
     * @return
     */
    public Velocity randomRotateWith(double rang) {
        this.rotate(random() * rang - rang / 2);
        return this;
    }

    /**
     * @param liv
     * @return
     */
    public Velocity changeVelocityTo(Organism liv) {
        if (liv != null) {
            Vector result = liv.getPosition().clone().subtract(this).setLength(this.getLength());
            this.setX(result.getX());
            this.setY(result.getY());
        }
        return this;
    }

    @Override
    public Velocity clone() {
        return new Velocity(this);
    }
    
    public void dummyMethod2() {
        super.dummyMethod1();
    }
}
